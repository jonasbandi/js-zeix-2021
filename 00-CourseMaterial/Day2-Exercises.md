## Preparation (API Endpoint)

Run the server providing the API endpoints for the exercises.

In the directory `08-Async/_server`:

```
npm install 
npm start
```

*Note:* `npm install` installs the dependencies. You have to run that only once.

The server can be stopped with `Ctrl-C`.

##### Word-API

This server provides a HTTP-based API from which you can get 9 words that make up a complete sentence. You can get the first word from the following URL:

```
http://localhost:3456/word/0
```

The last parameter can be varied between 0 and 8.

##### Number-Guess API

The sever picks a random number between 1 and 100. You have to guess the number.  
Send a guess to the server: 

```
POST http://localhost:3456/numberguess -- Body: { "number": 42 }
```

Retrieve the feedback about the guess:

```
GET http://localhost:3456/numberguess
```

The feedback contains the `status` of the guess: `BELOW`, `ABOVE` or `SUCCESS`.

*Hint:* To perform several parallel guesses you can pass a `clientId` like this:

```
POST http://localhost:3456/numberguess?clientId=12345
GET http://localhost:3456/numberguess?clientId=12345
```



You can use Postman (https://www.getpostman.com/) to play with the API.



## Exercise 1: AJAX with Callbacks

#### 1.1 Word API Client

Inspect the example `08-Async/10-Callbacks/91-Words-jQuery-callbacks`.

Open the web-page `index.html`. The logic in the script loads the first word from the server and renders it on the page.

Extend the example, so that all the words of the sentence are loaded from the server, and the complete sentence is rendered on the page.

Try two approaches for your implementation:

- All words are immediately queried from the server. The complete sentence should be rendered on the page once all the words have been loaded.

- Query one word after the other from the server. The next word should only be queried when the previous word has been received (observe the waterfall pattern in the network inspector of the developer tools). Each word should be rendered on the page as soon as it has been received from the server.

Use jQuery with callbacks to implement the logic.

#### 1.2 Number Guess API Client

Inspect the example `08-Async/10-Callbacks/91-NumberGuess-jQuery-callbacks`.

Implement the functionality for guessing the number:

- The user can submit a guess
- The applications shows if the guess is correct or if it is above or below

(Optional): Implement a "brute-force" approach where all the numbers are guessed automatically, and the matching number is then displayed.





## Exercise 2: AJAX with Promises
#### 1.1 Word API Client

Inspect the example `08-Async/20-Promises/91-Words-promise`.

Important: Do not use Internet Explorer to run this example since it does not implement native promises!

Inspect the web-page `index.html`. The logic in the script loads the first word from the server and renders it on the page.

Extend the example, so that all the words of the sentence are loaded from the server, and the complete sentence is rendered on the page.

Try two approaches for your implementation:

- All words are immediately queried from the server. The complete sentence should be rendered on the page once all the words have been loaded.

- Query one word after the other from the server. The next word should only be queried when the previous word has been received (observe the waterfall pattern in the network inspector of the developer tools). Each word should be rendered on the page as soon as it has been received from the server.

Use axios and Promises to implement the logic.

#### 1.2 Word API Client - Async/Await

Rewrite the solution from exercise 1.1 using `async` and `await`.

#### 1.3 Number Guess API Client

Inspect the example `08-Async/20-Promises/93-NumberGuesspromise-promise`.

Implement the functionality for guessing the number:

- The user can submit a guess
- The applications shows if the guess is correct or if it is above or below

(Optional): Implement a "brute-force" approach where all the numbers are guessed automatically, and the matching number is then displayed.




## Exercise 3: AJAX with Observables

#### 3.1 Word API Client

Inspect the example `08-Async/30-Observables/92-Words-exercise`.

Inspect the web-page `app/index.html`. The logic in the script loads the first word from the server and renders it on the page.

Implement the "waterfall" scenario: All words should be loaded one after the other from the server. The next word should only be queried when the previous word has been received.

Use axios and RxJS for the implementation.

#### 3.2 Number Guess API Client

Start the client in `99-Observables/95-NumberGuess-exercise`:

```
npm install
npm start
```

The client contains the boilerplate to talk to the server.	



##### Task 1:	Stateless Client

Implement the functionality for guessing the number:

- The user can submit a guess
- The applications shows if the guess is correct or if it is above or below

*Hint:* A possible solution uses `map` and `switchMap` ...

##### Task 2: Brute Force - Fast

The client should try all the numbers from 1-100 and then show the correct number.

*Hint:* A possible solution uses `map`, `mergeMap` and `filter`... `forkJoin` might also help ...	

##### Task 3: Brute Force - Sequential

The client should try all the numbers one after the other starting from 1. Once the number is found, no more guesses should be sent.

*Hint:* A possible solution uses `concatMap`, `switchMap`, `map`, `filter` and `take`
