import { ToDo, ToDoModel } from './model';

export function removeItem(model: ToDoModel, todo: ToDo) {
  model.removeToDo(todo);
}
