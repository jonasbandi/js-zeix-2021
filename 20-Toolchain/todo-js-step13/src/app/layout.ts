import $ from 'jquery';
import 'fittextjs';

export function applyLayout() {
  const $title = $('#title');
  $title.fitText(0.7);
}
