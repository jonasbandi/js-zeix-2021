/* eslint-env node */
module.exports = {
  env: {
    browser: true,
    es2021: true,
    jquery: true,
    jest: true
  },
  extends: [
    'eslint:recommended',
    'plugin:cypress/recommended',
    'plugin:@typescript-eslint/recommended'
  ],
  plugins: ['@typescript-eslint'],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaVersion: 2020,
    sourceType: 'module'
  },
  rules: {
    indent: ['error', 2],
    'linebreak-style': ['error', 'unix'],
    quotes: ['error', 'single'],
    semi: ['error', 'always'],
    '@typescript-eslint/explicit-member-accessibility': [0],
    '@typescript-eslint/explicit-module-boundary-types': [0]
  }
};
