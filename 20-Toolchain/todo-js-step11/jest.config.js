/* eslint-env node */

module.exports = {
  roots: ['src'],
  testEnvironment: 'jsdom',
  reporters: ['default', ['jest-junit', { outputDirectory: './build-info' }]]
};
