import $ from 'jquery';

class ToDo {
  constructor(title) {
    this.title = title;
  }
}

function updateNewToDoModel(title) {
  if (model.newToDo.title !== title) {
    model.newToDo.title = capitalize(title);
    notifyModelChange();
  }
}

function addToDo() {
  model.toDoList.push(model.newToDo);
  model.newToDo = new ToDo('');

  notifyModelChange();
}

function removeToDo(toDo) {
  model.toDoList = model.toDoList.filter(t => t !== toDo);
  notifyModelChange();
}

function notifyModelChange() {
  $model.trigger('modelchange');
}

function capitalize(value) {
  return value.charAt(0).toUpperCase() + value.slice(1);
}

const model = { updateNewToDoModel, addToDo, removeToDo };
model.newToDo = new ToDo('');
model.toDoList = [new ToDo('Learn JavaScript'), new ToDo('Learn React')];
const $model = $(model);

export { model };
