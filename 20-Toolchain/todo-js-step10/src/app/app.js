import '../css/index.css';
import 'core-js/stable/promise'; // needed for IE
import $ from 'jquery';
import { applyLayout } from './layout.js';
import { model } from './model';

console.log('Starting app ... ');

applyLayout();

const $app = $('#app');

$(model).on('modelchange', () => {
  $('#todo-text').val(model.newToDo.title);
  renderToDoList($('#todo-list'));
});

function renderForm() {
  const $input = $('<input>', {
    id: 'todo-text',
    placeholder: 'What do you want to do?'
  }).on('blur', () => {
    model.updateNewToDoModel($input.val());
  });

  $('<form>')
    .addClass('new-todo')
    .append(
      $input,
      $('<button>')
        .addClass('add-button')
        .text('+')
    )
    .on('submit', event => {
      event.preventDefault();
      model.updateNewToDoModel($input.val());
      model.addToDo();
    })
    .appendTo($app);
}

function renderToDoList($todoList) {
  $todoList.html('');

  for (const todo of model.toDoList) {
    $('<li>')
      .appendTo($todoList)
      .text(todo.title)
      .append(
        $('<button>')
          .text('X')
          .on('click', function() {
            import('./removeItem').then(m => {
              m.removeItem($(this));
            });
          })
      );
  }
}

function renderApp() {
  $app.html('');
  renderForm();
  const $todoListContainer = $('<div>')
    .addClass('todo-list-container')
    .appendTo($app);
  const $todoList = $('<ul>')
    .prop('id', 'todo-list')
    .addClass('todo-list')
    .appendTo($todoListContainer);
  renderToDoList($todoList);
}

renderApp();

function addToDo($input) {
  const textValue = $input.val();
  if (textValue) {
    $('<li>')
      .appendTo('#todo-list')
      .text(textValue)
      .append(
        $('<button>')
          .text('X')
          .on('click', function() {
            import('./removeItem').then(m => {
              m.removeItem($(this));
            });
          })
      );
    $input.val('');
  }
}
