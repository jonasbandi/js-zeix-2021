/// <reference types="cypress" />

context('ToDo List', () => {
    beforeEach(() => {
        cy.visit('http://localhost:5000/');
    });

    // https://on.cypress.io/interacting-with-elements

    it('adding a todo should extend the todo list', () => {
        cy.get('#todo-text').type('First ToDo');

        cy.get('#add-button').click();

        cy.get('#todo-list').find('li').should('have.length', 1);
    });
});
