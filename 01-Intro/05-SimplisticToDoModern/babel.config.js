/* eslint-env node */

var env = process.env.BABEL_ENV;

const presets = [
    [
        '@babel/env',
        {
            targets: {
                // ie: 11,
                edge: '17',
                firefox: '60',
                chrome: '80',
                safari: '11.1'
            },
            modules: env === 'test' ? 'commonjs' : false
        }
    ],
    '@babel/preset-typescript'
];

const plugins = ['@babel/plugin-proposal-class-properties'];

module.exports = { presets, plugins };
