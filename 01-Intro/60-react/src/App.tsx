import logo from './logo.svg';
import './App.css';
import { HelloWorld } from './HelloWorld';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo"/>
        <h1 className="App-title">Welcome to React</h1>
        <HelloWorld/>
      </header>
    </div>
  );
}

export default App;
